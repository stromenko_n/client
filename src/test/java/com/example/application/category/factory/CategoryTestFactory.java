package com.example.application.category.factory;

import com.example.core.data.category.entity.CategoryDoc;
import org.bson.types.ObjectId;

public class CategoryTestFactory {

    public static CategoryDoc createCategory(String name) {
        CategoryDoc categoryDoc = new CategoryDoc();
        categoryDoc.setId(ObjectId.get());
        categoryDoc.setName(name);

        return categoryDoc;
    }

}
