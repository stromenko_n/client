package com.example.application.category;

import com.example.application.category.factory.CategoryTestFactory;
import com.example.client.ClientApplication;
import com.example.client.category.root.CategoryRoot;
import com.example.core.data.category.repository.CategoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ClientApplication.class)
@AutoConfigureMockMvc
public class CategoryTest {
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void createCategory() {
        categoryRepository.save(CategoryTestFactory.createCategory("one"));
        categoryRepository.save(CategoryTestFactory.createCategory("two"));
        categoryRepository.save(CategoryTestFactory.createCategory("three"));
        categoryRepository.save(CategoryTestFactory.createCategory("four"));
        categoryRepository.save(CategoryTestFactory.createCategory("five"));
    }

    @Test
    public void findAll() throws Exception {
        mockMvc.perform(get("/" + CategoryRoot.FIND_ALL))
                .andExpect(jsonPath("$.count", is(5)))
                .andExpect(jsonPath("$.categories", hasSize(5)))
                .andExpect(jsonPath("$.categories[*].name", contains("one", "two", "three", "four", "five")))
                .andExpect(status().isOk());
    }

}
