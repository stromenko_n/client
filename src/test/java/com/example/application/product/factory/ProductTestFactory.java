package com.example.application.product.factory;

import com.example.core.data.category.entity.CategoryDoc;
import com.example.core.data.category.model.CategoryDTO;
import com.example.core.data.category.repository.CategoryRepository;
import com.example.core.data.product.entity.ProductDoc;
import com.example.core.data.product.model.ProductDTO;
import com.example.core.data.product.repository.ProductRepository;
import com.example.core.domain.enititys.category.service.CategoryService;
import com.example.core.domain.enititys.product.service.ProductService;
import org.bson.types.ObjectId;

import java.util.Collections;

public class ProductTestFactory {

    public static void createProduct(ProductService productService, String id) {
        productService.dropCollection();
        productService.create(new ProductDTO(null, "test", "one", null, Collections.emptyList(), id, null));
        productService.create(new ProductDTO(null, "test", "two", null, Collections.emptyList(), id, null));
        productService.create(new ProductDTO(null, "test", "three", null, Collections.emptyList(), id, null));
        productService.create(new ProductDTO(null, "test", "four", null, Collections.emptyList(), ObjectId.get().toString(), null));
        productService.create(new ProductDTO(null, "test", "five", null, Collections.emptyList(), id, null));
    }

    public static CategoryDoc createCategory(CategoryService service) {
        service.dropCollection();
        CategoryDTO dto = new CategoryDTO();
        dto.setId(ObjectId.get().toString());

        service.create(dto);
        return dto.toDoc();
    }

}
