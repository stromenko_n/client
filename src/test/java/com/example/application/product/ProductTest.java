package com.example.application.product;

import com.example.application.product.factory.ProductTestFactory;
import com.example.client.ClientApplication;
import com.example.client.product.root.ProductRoot;
import com.example.core.data.category.entity.CategoryDoc;
import com.example.core.data.category.repository.CategoryRepository;
import com.example.core.data.product.repository.ProductRepository;
import com.example.core.domain.enititys.category.service.CategoryService;
import com.example.core.domain.enititys.product.service.ProductService;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.contains;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ClientApplication.class)
@AutoConfigureMockMvc
public class ProductTest {
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void findByCriteria() throws Exception {
        CategoryDoc categoryDoc = ProductTestFactory.createCategory(categoryService);
        ProductTestFactory.createProduct(productService, categoryDoc.getId().toString());

        mockMvc
                .perform(get("/" + ProductRoot.FIND_BY_CATEGORY.replace("{categoryId}", categoryDoc.getId().toString())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.products", hasSize(4)))
                .andExpect(jsonPath("$.count", is(4)))
                .andExpect(jsonPath("$.products[*].description", contains("one", "two", "three", "five")));
    }

    @Test
    public void findAll() throws Exception {
        ProductTestFactory.createProduct(productService, ObjectId.get().toString());

        mockMvc
                .perform(get("/" + ProductRoot.FIND_ALL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.count", is(5)))
                .andExpect(jsonPath("$.products", hasSize(5)))
                .andExpect(jsonPath("$.products[*].description", contains("one", "two", "three", "four", "five")));
    }

}
