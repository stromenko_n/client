package com.example.client.category.root;

import com.example.core.base.root.ClientRoot;

public class CategoryRoot {
    private static final String BASE = ClientRoot.CLIENT + "/category";

    public static final String FIND_ALL = BASE + "/find_all";

}
