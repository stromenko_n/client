package com.example.client.category.api.response;

import com.example.core.data.category.model.CategoryDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CategoryResponse {
    private List<CategoryDTO> categories;

    private Integer count;

    public CategoryResponse(List<CategoryDTO> dto) {
        categories = dto;
        count = dto.size();
    }

}
