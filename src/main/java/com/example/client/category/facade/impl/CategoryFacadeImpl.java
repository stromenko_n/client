package com.example.client.category.facade.impl;

import com.example.client.category.api.response.CategoryResponse;
import com.example.client.category.facade.CategoryFacade;
import com.example.core.data.category.model.CategoryDTO;
import com.example.core.domain.enititys.category.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryFacadeImpl implements CategoryFacade {
    private final CategoryService service;

    @Override
    public CategoryResponse findAll() {
        List<CategoryDTO> categoryDTOS = service.findAll();

        return new CategoryResponse(categoryDTOS);
    }

}
