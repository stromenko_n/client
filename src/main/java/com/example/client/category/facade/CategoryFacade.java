package com.example.client.category.facade;

import com.example.client.category.api.response.CategoryResponse;

public interface CategoryFacade {

    CategoryResponse findAll();
}
