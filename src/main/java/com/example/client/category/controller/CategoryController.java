package com.example.client.category.controller;

import com.example.client.category.api.response.CategoryResponse;
import com.example.client.category.facade.CategoryFacade;
import com.example.client.category.root.CategoryRoot;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryFacade facade;

    @GetMapping(CategoryRoot.FIND_ALL)
    public CategoryResponse findAll() {
        return facade.findAll();
    }

}
