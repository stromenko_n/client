package com.example.client.product.root;

import com.example.core.base.root.ClientRoot;

public class ProductRoot {

    private static final String BASE = ClientRoot.CLIENT + "/product";

    public static final String FIND_BY_CATEGORY = BASE + "/find_by_category/{categoryId}";

    public static final String FIND_ALL = BASE + "/find_all";

}
