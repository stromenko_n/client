package com.example.client.product.facade;

import com.example.client.product.api.response.ProductResponse;
import org.bson.types.ObjectId;

public interface ProductFacade {

    ProductResponse findByCategory(ObjectId categoryId);

    ProductResponse findAllProducts();

}
