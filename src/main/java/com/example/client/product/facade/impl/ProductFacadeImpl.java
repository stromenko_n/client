package com.example.client.product.facade.impl;

import com.example.client.product.api.response.ProductResponse;
import com.example.client.product.facade.ProductFacade;
import com.example.core.domain.enititys.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductFacadeImpl implements ProductFacade {
    private final ProductService productService;

    @Override
    public ProductResponse findByCategory(ObjectId categoryId) {
        return new ProductResponse(productService.findByCategory(categoryId));
    }

    @Override
    public ProductResponse findAllProducts() {
        return new ProductResponse(productService.findAllProducts());
    }

}
