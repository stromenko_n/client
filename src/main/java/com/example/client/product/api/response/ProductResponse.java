package com.example.client.product.api.response;

import com.example.core.data.product.model.ProductDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ProductResponse {
    private List<ProductDTO> products;

    private Integer count;

    public ProductResponse(List<ProductDTO> dto) {
        products = dto;
        count = dto.size();
    }

}
