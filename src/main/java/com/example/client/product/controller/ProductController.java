package com.example.client.product.controller;

import com.example.client.product.api.response.ProductResponse;
import com.example.client.product.facade.ProductFacade;
import com.example.client.product.root.ProductRoot;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ProductController {
    private final ProductFacade facade;

    @GetMapping(value = ProductRoot.FIND_BY_CATEGORY)
    public ProductResponse findByCategory(@PathVariable ObjectId categoryId) {
        return facade.findByCategory(categoryId);
    }

    @GetMapping(value = ProductRoot.FIND_ALL)
    public ProductResponse findAll() {
        return facade.findAllProducts();
    }

}
